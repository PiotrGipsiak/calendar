import {simulateAsyncCall} from './fake-backend.js'

const startTimeInput = flatpickr('#eventStartTime', {
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
    time_24hr: true,
    allowInput: true
});

const endTimeInput = flatpickr('#eventEndTime', {
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
    time_24hr: true,
    allowInput: true
});

let selectedStartDate;
let selectedEndDate;
let selectedId;
let edited;

let eventNameInput = document.querySelector('#eventName');
const calendarEl = document.querySelector('#calendar');
const saveButton = document.querySelector('#saveButton');
const deleteButton = document.querySelector('#deleteButton');
const closeButton = document.querySelector('#closeButton');
const errorWrapper = document.querySelector(".error-wrapper");

saveButton.addEventListener('click', saveEvent);
deleteButton.addEventListener('click', deleteEvent);
closeButton.addEventListener('click', closeModal);

const inputs = [eventNameInput, startTimeInput.input, endTimeInput.input];

inputs.forEach(input => {
    input.addEventListener('input', (e) => {
        if (!(e.target.value === '')) {
            input.classList.remove('is-invalid');
        } else {
            input.classList.add('is-invalid');
        }
    })
});


const calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: ['interaction', 'dayGrid', 'timeGrid'],
    selectable: true,
    locale: 'pl',
    firstDay: 1,
    selectConstraint: {
        start: new Date().setDate(new Date().getDate() - 1)
    },
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    select: (event) => {
        createEvent(event);
        openModal();
    },
    eventClick: ({event}) => {
        editEvent(event);
        openModal();
    }

});

calendar.render();
getData();

function saveEvent() {
    inputs.forEach(input => {
        if (!input.value) {
            input.classList.add('is-invalid');
        }
    });


    if (startTimeInput.input.classList.contains('is-invalid') || endTimeInput.input.classList.contains('is-invalid') || eventNameInput.classList.contains('is-invalid')) {
        return;
    }

    if (endTimeInput.input.value === startTimeInput.input.value) {
        errorWrapper.innerText = `Godzina końca jest taka sama jak  godzina początku wydarzenia!!!`;
        errorWrapper.classList.add('invalid-dates');
        return;
    }

    if (endTimeInput.input.value < startTimeInput.input.value) {
        errorWrapper.innerText = `Godzina końca jest wcześniejsza od godziny początku wydarzenia!!!`;
        errorWrapper.classList.add('invalid-dates');
        return;
    }

    const substractedDate = new Date(new Date(selectedEndDate).setDate(new Date(selectedEndDate).getDate() - 1)).toISOString().split('T')[0];

    const createNewEventData = {
        title: eventNameInput.value,
        start: selectedStartDate + 'T' + startTimeInput.input.value,
        end: substractedDate + 'T' + endTimeInput.input.value
    };

    const updateEventData = {
        id: selectedId,
        title: eventNameInput.value,
        start: selectedStartDate + 'T' + startTimeInput.input.value,
        end: selectedEndDate + 'T' + endTimeInput.input.value
    };

    errorWrapper.innerHTML = '';

    if (edited) {
        simulateAsyncCall({method: 'put', data: updateEventData}).then(response => {
            calendar.removeAllEvents();
            calendar.addEventSource(response);
        });
    } else {
        simulateAsyncCall({method: 'post', data: createNewEventData}).then(response => {
            calendar.removeAllEvents();
            calendar.addEventSource(response);
        });
    }

    closeModal();
}

function createEvent(event) {
    if (event.allDay) {
        startTimeInput.setDate('');
        endTimeInput.setDate('');
    } else {
        startTimeInput.setDate(event.start);
        endTimeInput.setDate(event.end);
    }

    selectedStartDate = event.startStr;
    selectedEndDate = event.endStr;
}

function editEvent(event) {
    edited = true;
    selectedStartDate = calendar.formatIso(event.start).split('T')[0];
    selectedEndDate = calendar.formatIso(event.end).split('T')[0];
    selectedId = event.id;
    eventNameInput.value = event.title;
    startTimeInput.setDate(event.start);
    endTimeInput.setDate(event.end);
}


function deleteEvent() {
    simulateAsyncCall({method: 'delete', data: {id: selectedId}}).then(response => {
        calendar.removeAllEvents();
        calendar.addEventSource(response);
    });
    closeModal()
}

function getData() {
    simulateAsyncCall({method: 'get'}).then(response => {
        calendar.addEventSource(response);
    });
}

function clearInputs() {
    eventNameInput.value = '';
    startTimeInput.setDate('');
    endTimeInput.setDate('');
}

function openModal() {
    $("#myModal").modal({backdrop: 'static'})
}

function closeModal() {
    $("#myModal").modal('hide');
    clearInputs();
    edited = false;
}

