let dataBase = [
    {
        id: '1', start: '2020-02-20T19:00', end: '2020-02-20T21:00', title: 'Rezerwacja boiska 7N'
    }
];

function generateID() {
    return '_' + Math.random().toString(36).substr(2, 9);
}

export function simulateAsyncCall(request) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            switch (request.method) {
                case 'get': {
                    resolve(dataBase);
                    break;
                }
                case 'post': {
                    dataBase = [...dataBase, {id: generateID(), ...request.data}];
                    resolve(dataBase);
                    break;
                }
                case 'put': {
                    dataBase = dataBase.map(event => {
                        if (event.id === request.data.id) {
                            return {...event, ...request.data}
                        } else {
                            return event
                        }
                    });
                    resolve(dataBase);
                    break;
                }
                case 'delete': {
                    dataBase = dataBase.filter(event => event.id !== request.data.id);
                    resolve(dataBase);
                    break;
                }
                default :
                    resolve({status: 400, message: 'Bad Request'})
            }
        }, 500)
    })
}
